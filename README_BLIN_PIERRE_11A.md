# TP n°2 Bash

## À qui est ce TP ?
Nom : BLIN 

Prénom : Pierre

Groupe : 11A



# Question n°1:

Afin de connaître le nombre total de fichiers et de dossiers dans notre arborescence, nous pouvons exécuter la commande suivante : 
- `ls -R | wc -l`

    ![Nombre total de fichiers et de dossiers](./Images/q1.png)

# Question n°2 :
Pour donner le nombre de fichiers et répertoires dans l'arborescence en les distinguants, on peut exécuter cette commande : 
- `find . -type f | wc -l` : Afin de compter le nombre de fichiers

    ![Q2](./Images/q2.png)


- `find . -type d | wc-l` : Afinde compter le nombre de dossiers

    ![Q2.2](./Images/q2.2.png)

# Question n°3 :
La commande suivante va nous permettre de chercher et liter tous les fichiers ayant dans leur nom "jpg" avec une certaine clarté puisqu'il nous donne juste le nom du fichier : 

- `ls -R | grep -E *.jpg` : Cette commande nous donne plus de clarté sur le résultat que l'on souhaite obtenir.

    ![Lister les fichiers jpg avec plus de clarté](./Images/q3.png)

# Question n°4 :
Si nous voulons que le terminal nous affiche seulement les fichiers ayant pour extension jpg ou png, nous devons exécuter la commande suivante :

- `ls -R | grep -E *(.jpg|.png)`

    ![Lister seulement les fichiers jpg ou png](./Images/q4.png)

# Question n°5 : 
Je vous montre ci-dessous le script et le résultat que j'ai obtenu mais bien évidemment je le met en pièce jointe afin que vous puissiez jeter un coup d'oeil par vous-mêmes. Le fichier où se trouve le résultat se nomme scr5.txt :

![Script 5](./Images/q5.png)

![Script 5.2](./Images/q5.2.png)

# Question n°6 :
Comme pour la question précédente, je vais vous montrer une image de mon script et du résultat mais je vais également le fournir en pièce jointe :

![Script 6](./Images/q6.png)

![Script 6.2](./Images/q6.2.png)

# Question n°7 :
Afin de renvoyer le listing de tous les fichiers dans cette arborescence se terminant par l’extension jpg quel que soit la case des caractères 'j', 'p' et 'g', nous allons utiliser la commande suivante : 

- `ls -R | grep -i "jpg"`

![Script 7](./Images/q7.png)

# Question n°8 :
Pour pouvoir lister seulement les fichiers munis de l’extension jpg, il faut créer un script ne prenant en compte que ce type de fichier, nous allons donc construire notre script de cette manière (script fourni en pièce jointe):

![Script 8](./Images/q8.png)

Notre but est de créer premièrement le répertoire outputs qui va accueillir les fichiers jpg. Puis, il faut chercher dans notre arborescence, le type de fichier que l’on veut, ici les jpg, avec la commande « find » et en rajoutant les options «type» et «name» et «f» afin que il sélectionne bien que les fichiers avec dans leur nom « jpg ». Ensuite, nous allons « move » ces fichiers à l’aide de la commande mv dans notre dossier outputs que nous avons créer précédemment.

![Script 8.2](./Images/q8.2.png)

# Question n°9 : 
(Script fourni en pièce jointe) 

Afin de réaliser ce qu’il nous ait demandé, nous allons juste modifier le script que nous avons utilisé précédemment. Nous allons juste y ajouter  la/les ligne(s) de commande(s) qu’il nous faut afin de changer les droits de nos fichiers images :

![Script 9](./Images/q9.png)

Nous avons remplacer la ligne de commande « mv $variable outputs/ » par « chmod 600 $variable » afin de changer les droits pour que les fichiers n’ai plus que le droit de lecture et écriture.
Nous avons également supprimer la seconde ligne où nous décidons de créer le fichier puisqu’il existe déjà.

# Question n°10 :

Si nous voulons afficher la taille de tous les fichiers de l'arborescence, nous allons simplement exécuter la commande :
- `du -sh`

Sinon, pour afficher la taille de seulement tous les fichiers images, nous allons exécuter cette commande :

![Cmd 10](./Images/q10.png)

Nous obtenons en unité lisible, 2,6Go. Si l'on ne précise pas l'option `-h`, nous obtenons 2 702 504 Ko.


# Information supplémentaire
## Dépôt GitLab

> Je vous mets à disposition le lien vers mon dépôt GitLab en lien avec ce second TP de Bash :

    https://gitlab.com/pblin28/tp2_bash


